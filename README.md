将 TrueCharts 添加到您的 SCALE ：

顶级菜单转到《应用》页面

在 《应用》 页面上选择 《管理目录》 选项卡

单击《添加目录》：

阅读 iXsystems 通知后，单击继续并输入所需信息：
```
名称：truecharts

存储库：https://github.com/truecharts/catalog

首选列车：enterprise 和 stable

分支：main
```

单击保存并允许 SCALE 使用 TrueCharts 刷新其目录（这可能需要几分钟）